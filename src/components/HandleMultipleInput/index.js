import React from 'react';

class HandleMultipleInput extends React.Component {
  constructor() {
    super();
    this.state = {
      checkbox: false,
      counter: 0,
      firstName: ''
    }
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange = event => {
    const target = event.target;
    console.log('target', target.name)

    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    console.log(`typeof(values): ${typeof (value)} - value: ${value}`)

    this.setState({
      [name]: value
    });

    // this is checking is state resert complete
    this.setState({
      aa: 1234
    });
  }

  render() {
    return (
      <form>
        <label>
          checkbox :
          <input name='checkbox' defaultChecked={this.state.checkbox} value={this.state.checkbox} onChange={this.handleInputChange} type='checkbox' />
        </label>
        <label>
          input data:
          <input name='counter' value={this.state.counter} onChange={this.handleInputChange} type='number' />
        </label>
        <label>
          your first name:
          <input name='firstName' value={this.state.firstName} onChange={this.handleInputChange} type='text' />
        </label>
      </form>
    )
  }
}


export default HandleMultipleInput;