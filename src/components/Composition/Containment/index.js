import FancyBorder from './FancyBorder';

const WelcomeDialog = () => {
  return (
    <FancyBorder color="blue"
      left={<h1>this is h1 with left</h1>}
      right={<h1>this is h1 with right</h1>}
    >
      <h1 className="Dialog-title">
        Welcome
      </h1>
      <p className="Dialog-message">
        Thank you for visiting our spacecraft!
      </p>
    </FancyBorder>
  );
}

export default WelcomeDialog;