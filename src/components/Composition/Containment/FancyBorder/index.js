// Containment Some components don’t know their children ahead of time.
//This is especially common for components like Sidebar or Dialog that represent generic “boxes”.

const FancyBorder = (props) => {
  return (
    <div className={'FancyBorder FancyBorder-' + props.color}>
      {props.children}
      <br />
      {props.left}
      <br />
      {props.right}
    </div>
  );
}

export default FancyBorder;