import React from 'react';

// With a controlled component, the input’s value is always driven by the React state
class BoilingVerdict extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.celsius >= 100) {
      return <p>The water would boil.</p>;
    }
    return <p>The water would not boil.</p>;
  }
}

export default BoilingVerdict;