import React from 'react';

// With a controlled component, the input’s value is always driven by the React state
class ControlledComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = { value: '' }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange = (event) => {
    console.log(event)
    this.setState({
      value: event.target.value
    });
  }

  handleSubmit = (event) => {
    alert('hi' + this.state.value)
    event.preventDefault();
  }

  handleReset = (event) => {
    console.log(event)
    this.setState({ value: 'reseted' })
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} onReset={this.handleReset}>
        <label>
          label:
          <input type='text' value={this.state.value} onChange={this.handleInputChange} />
        </label>
        <button type='submit'>Submit</button>
        <button type='reset'>Reset</button>
      </form>
    )
  }
}

export default ControlledComponent;