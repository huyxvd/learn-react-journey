import React from 'react';

class TextArea extends React.Component {
  constructor(props) {
    super(props);

    this.state = { textValue: '' }

    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit = event => {
    alert('text area = ' + this.state.textValue);
    event.preventDefault();
  }
  handleTextChange = event => {
    this.setState({ textValue: event.target.value })
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Your name:
          <textarea value={this.state.textValue} onChange={this.handleTextChange} />
        </label>
        <button type='submit'>send</button>
      </form>
    )
  }
}

export default TextArea;