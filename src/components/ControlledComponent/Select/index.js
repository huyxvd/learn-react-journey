import React from 'react';

// todo multiple selected & default select are none
class Select extends React.Component {
  constructor() {
    super();

    this.state = { selected: 'cononut' }
    this.handleSelect = this.handleSelect.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSelect = event => {
    console.log(event.target)
    this.setState({ selected: event.target.value });
  };

  handleSubmit = event => {
    alert('selected values = ' + this.state.selected);
    event.preventDefault();
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          select fruit that you like most:
          <select onChange={this.handleSelect} value={this.state.selected}>
            <option value='apple'>apple</option>
            <option value='banana'>banana</option>
            <option value='mango'>mango</option>
            <option value='cononut'>cononut</option>
          </select>
        </label>
        <button type='submit'>submit</button>
      </form>
    )
  }
}

export default Select;