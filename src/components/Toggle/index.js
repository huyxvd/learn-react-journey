import React from 'react';

class Toggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isToggleOn: true
    }

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(prev => ({
      isToggleOn: !prev.isToggleOn
    }));
  }

  render() {
    return <>
      <button onClick={this.handleClick}>toggle</button>
      <h1>toggle is: {this.state.isToggleOn ? 'On' : 'Off'}</h1>
    </>
  }
}

export default Toggle;