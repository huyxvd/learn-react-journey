import React from 'react';

const TodoItem = ({ id, isComplete, title, onClick }) => {
  return (
    <p style={{ color: isComplete ? 'green' : 'red' }} onClick={() => onClick(id)}>
      {title}
    </p>
  )
}

export default TodoItem;