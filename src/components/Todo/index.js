import React, { useState } from 'react';
import Item from './item';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import AddIcon from '@mui/icons-material/Add';
import Fab from '@mui/material/Fab';

const initialData = [
  { id: 1, title: 'hello', isComplete: true },
  { id: 2, title: 'hello there' }
];

const Todo = () => {
  const [todoItems, setTodoItems] = useState(initialData);
  const [itemValue, setItemValue] = useState('');

  const onHandleItemClick = (id) => {
    const newData = todoItems.map(x => {
      if (x.id === id) {
        x.isComplete = !x.isComplete;
      }
      return x;
    });
    setTodoItems(newData);
  }

  const onAddNewItem = () => {
    if (itemValue) {
      setTodoItems(pre => {
        const newArr = [...pre];
        newArr.push({ id: pre.length + 1, title: itemValue });
        return newArr;
      })
      setItemValue('');
    }
  }

  const inputOnChange = (e) => {
    //console.log(e.target.value);
    setItemValue(e.target.value)
  }

  return (
    <>
      <h1>todo app</h1>
      <Box
        component='form'
        noValidate
        autoComplete='off'
      >
        <TextField id='outlined-basic' label='new todo thing' variant='outlined' value={itemValue} onChange={e => inputOnChange(e)} />
      </Box>
      <Box>
        <Fab color="primary" aria-label="add" onClick={onAddNewItem}>
          <AddIcon />
        </Fab>
      </Box>
      {todoItems.map(item => <Item {...item} key={item.id} onClick={onHandleItemClick} />)}
    </>
  )
};

export default Todo;