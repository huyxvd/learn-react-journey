import React from 'react';

// With a controlled component, the input’s value is always driven by the React state
class UnControlledComponent extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.input = React.createRef();
    this.name = React.createRef();
  }

  handleSubmit = (event) => {
    console.log('this.input', this.input)
    alert(`input: ${this.input.current.value}, name: ${this.name.current.value}`)
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          uncontrolled component:
          <input ref={this.input}/>
        </label>
        <label>
          name:
          <input ref={this.name}/>
        </label>
        <button type='submit'>Submit</button>
      </form>
    )
  }
}

export default UnControlledComponent;