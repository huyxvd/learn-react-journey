import React from 'react';

class Clock extends React.Component {
  constructor(props) {
    super(props);

    // this statement only call in constructor
    this.state = {
      date: new Date()
    }
  }

  // after render first time this one will be call
  componentDidMount() {
    // set the timers to call the setState every 1000 mili sec. then update the values of date by new date
    this.timerId = setInterval(() => {
      this.setState({
        date: new Date()
      })
    }, 1000);
  }

  componentWillUnmount() {
    // clear the timer when component no longer used it
    clearInterval(this.timerId);
  }

  render() {
    return (
      <h1>It's {this.state.date.toLocaleString()}</h1>
    )
  }
}

export default Clock;