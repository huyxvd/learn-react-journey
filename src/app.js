import React from 'react';
import Clock from './components/Clock';
import Toggle from './components/Toggle';
import ControlledComponent from './components/ControlledComponent';
import TextArea from './components/ControlledComponent/TextArea';
import Select from './components/ControlledComponent/Select';
import HandleMultipleInput from './components/HandleMultipleInput';
import UnControlledComponent from './components/UnControlledComponent';
import Calculator from './components/LiftingStateUp/Calculator';
import WelcomeDialog from './components/Composition/Containment';
import Todo from './components/Todo';

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <>
      <Todo />
    </>
  }
}

export default App;